export class DestinoFestival {
    nombre:string;
    lugar:string;
    imagenUrl:string;

    constructor(n:string, l:string, u:string){
        this.nombre = n;
        this.lugar = l;
        this.imagenUrl = u;
    }
}
