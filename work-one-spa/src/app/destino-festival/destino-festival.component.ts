import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { DestinoFestival } from '../models/destinofestival.model';

@Component({
  selector: 'app-destino-festival',
  templateUrl: './destino-festival.component.html',
  styleUrls: ['./destino-festival.component.css']
})
export class DestinoFestivalComponent implements OnInit {
  @Input() destino: DestinoFestival;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() { }

  ngOnInit(): void {
  }

}
