import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DestinoFestivalComponent } from './destino-festival.component';

describe('DestinoFestivalComponent', () => {
  let component: DestinoFestivalComponent;
  let fixture: ComponentFixture<DestinoFestivalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DestinoFestivalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DestinoFestivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
