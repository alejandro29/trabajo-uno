import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaFestivalComponent } from './lista-festival.component';

describe('ListaFestivalComponent', () => {
  let component: ListaFestivalComponent;
  let fixture: ComponentFixture<ListaFestivalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaFestivalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaFestivalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
