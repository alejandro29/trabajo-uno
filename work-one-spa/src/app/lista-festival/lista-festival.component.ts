import { Component, OnInit } from '@angular/core';
import { DestinoFestival } from './../models/destinofestival.model';
import { ConstantPool } from '@angular/compiler';

@Component({
  selector: 'app-lista-festival',
  templateUrl: './lista-festival.component.html',
  styleUrls: ['./lista-festival.component.css']
})
export class ListaFestivalComponent implements OnInit {

  destinos: DestinoFestival[];

  constructor() {
    this.destinos = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, lugar:string, url:string):boolean{
    this.destinos.push(new DestinoFestival(nombre, lugar, url));
    console.log(new DestinoFestival(nombre, lugar, url));
    return false;
  }

}
