import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoFestivalComponent } from './destino-festival/destino-festival.component';
import { ListaFestivalComponent } from './lista-festival/lista-festival.component';

@NgModule({
  declarations: [
    AppComponent,
    DestinoFestivalComponent,
    ListaFestivalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
